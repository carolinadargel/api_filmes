from django.shortcuts import render, redirect
from django.views import View
from .models import Film
import ipdb

# Create your views here.

class FilmsListView(View):
    def get(self,request):
        queryset = Film.objects.all()
        return render(request, 'films/film_list.html', {'films': queryset})
    
class FilmDescriptionView(View):
    def get(self, request, **kwargs):
        film_id = kwargs['film_id']
        film = Film.objects.get(id=film_id)    
             
        return render(request, 'films/film_description.html', {'film': film})


class FilmCreateView(View):
    def get(self, request):  
        return render(request, 'films/film_form.html')
    
    
    def post(self, request):
                
        title = request.POST['title']
        description = request.POST['description']
        user = request.POST['user']
        
        Film.objects.create(title=title, description=description, user=user)
        
        return redirect('/films/')
    
# class FilmByUser(View):
#     def get(self, request, **kwargs):  
#         user = kwargs['user']
#         film = Film.objects.get(user=user)
#         print(film)
#         return render(request, 'films/film_list.html', {'film': film})
