from django.urls import path
from .views import FilmsListView, FilmDescriptionView, FilmCreateView, FilmByUser



urlpatterns = [
    path('films/', FilmsListView.as_view()),
    path('film/<int:film_id>/', FilmDescriptionView.as_view()),
    path('films/create/', FilmCreateView.as_view()),
    path('films/<user>/', FilmByUser.as_view())
]

